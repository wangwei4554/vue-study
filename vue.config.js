module.exports = {
    configureWebpack: {
        devServer: {
            before(app) {
                app.get('/api/goods', function (req, res) {
                    res.json({
                        list: [
                            { id: 1, name: "java工程师", wage: 1000 },
                            { id: 2, name: "android工程师", wage: 1000 },
                            { id: 3, name: "web工程师", wage: 1000 },
                            { id: 4, name: "python工程师", wage: 1000 }
                        ]
                    });
                })
            }
        }

    }
}