[33mcommit 10510ba5ff56f25572cb00adffe4c9e5f9760190[m[33m ([m[1;36mHEAD -> [m[1;32mmaster[m[33m)[m
Author: wangwei <邮箱地址455472143@qq.com>
Date:   Wed Sep 25 14:42:28 2019 +0800

    init

[1mdiff --git a/.gitignore b/.gitignore[m
[1mnew file mode 100644[m
[1mindex 0000000..a0dddc6[m
[1m--- /dev/null[m
[1m+++ b/.gitignore[m
[36m@@ -0,0 +1,21 @@[m
[32m+[m[32m.DS_Store[m
[32m+[m[32mnode_modules[m
[32m+[m[32m/dist[m
[32m+[m
[32m+[m[32m# local env files[m
[32m+[m[32m.env.local[m
[32m+[m[32m.env.*.local[m
[32m+[m
[32m+[m[32m# Log files[m
[32m+[m[32mnpm-debug.log*[m
[32m+[m[32myarn-debug.log*[m
[32m+[m[32myarn-error.log*[m
[32m+[m
[32m+[m[32m# Editor directories and files[m
[32m+[m[32m.idea[m
[32m+[m[32m.vscode[m
[32m+[m[32m*.suo[m
[32m+[m[32m*.ntvs*[m
[32m+[m[32m*.njsproj[m
[32m+[m[32m*.sln[m
[32m+[m[32m*.sw?[m
[1mdiff --git a/README.md b/README.md[m
[1mnew file mode 100644[m
[1mindex 0000000..c843318[m
[1m--- /dev/null[m
[1m+++ b/README.md[m
[36m@@ -0,0 +1,29 @@[m
[32m+[m[32m# vue-study[m
[32m+[m
[32m+[m[32m## Project setup[m
[32m+[m[32m```[m
[32m+[m[32mnpm install[m
[32m+[m[32m```[m
[32m+[m
[32m+[m[32m### Compiles and hot-reloads for development[m
[32m+[m[32m```[m
[32m+[m[32mnpm run serve[m
[32m+[m[32m```[m
[32m+[m
[32m+[m[32m### Compiles and minifies for production[m
[32m+[m[32m```[m
[32m+[m[32mnpm run build[m
[32m+[m[32m```[m
[32m+[m
[32m+[m[32m### Run your tests[m
[32m+[m[32m```[m
[32m+[m[32mnpm run test[m
[32m+[m[32m```[m
[32m+[m
[32m+[m[32m### Lints and fixes files[m
[32m+[m[32m```[m
[32m+[m[32mnpm run lint[m
[32m+[m[32m```[m
[32m+[m
[32m+[m[32m### Customize configuration[m
[32m+[m[32mSee [Configuration Reference](https://cli.vuejs.org/config/).[m
[1mdiff --git a/babel.config.js b/babel.config.js[m
[1mnew file mode 100644[m
[1mindex 0000000..ba17966[m
[1m--- /dev/null[m
[1m+++ b/babel.config.js[m
[36m@@ -0,0 +1,5 @@[m
[32m+[m[32mmodule.exports = {[m
[32m+[m[32m  presets: [[m
[32m+[m[32m    '@vue/app'[m
[32m+[m[32m  ][m
[32m+[m[32m}[m
[1mdiff --git a/package-lock.json b/package-lock.json[m
[1mnew file mode 100644[m
[1mindex 0000000..1ec41e5[m
[1m--- /dev/null[m
[1m+++ b/package-lock.json[m
[36m@@ -0,0 +1,11591 @@[m
[32m+[m[32m{[m
[32m+[m[32m  "name": "vue-study",[m
[32m+[m[32m  "version": "0.1.0",[m
[32m+[m[32m  "lockfileVersion": 1,[m
[32m+[m[32m  "requires": true,[m
[32m+[m[32m  "dependencies": {[m
[32m+[m[32m    "@babel/code-frame": {[m
[32m+[m[32m      "version": "7.5.5",[m
[32m+[m[32m      "resolved": "https://registry.npm.taobao.org/@babel/code-frame/download/@babel/code-frame-7.5.5.tgz",[m
[32m+[m[32m      "integrity": "sha1-vAeC9tafe31JUxIZaZuYj2aaj50=",[m
[32m+[m[32m      "dev": true,[m
[32m+[m[32m      "requires": {[m
[32m+[m[32m        "@babel/highlight": "^7.0.0"[m
[32m+[m[32m      }[m
[32m+[m[32m    },[m
[32m+[m[32m    "@babel/core": {[m
[32m+[m[32m      "version": "7.6.2",[m
[32m+[m[32m      "resolved": "https://registry.npm.taobao.org/@babel/core/download/@babel/core-7.6.2.tgz?cache=0&sync_timestamp=1569275100044&other_urls=https%3A%2F%2Fregistry.npm.taobao.org%2F%40babel%2Fcore%2Fdownload%2F%40babel%2Fcore-7.6.2.tgz",[m
[32m+[m[32m      "integrity": "sha1-Bpp3bo1enu//diNryIRVZr0x3ZE=",[m
[32m+[m[32m      "dev": true,[m
[32m+[m[32m      "requires": {[m
[32m+[m[32m        "@babel/code-frame": "^7.5.5",[m
[32m+[m[32m        "@babel/generator": "^7.6.2",[m
[32m+[m[32m        "@babel/helpers": "^7.6.2",[m
[32m+[m[32m        "@babel/parser": "^7.6.2",[m
[32m+[m[32m        "@babel/template": "^7.6.0",[m
[32m+[m[32m        "@babel/traverse": "^7.6.2",[m
[32m+[m[32m        "@babel/types": "^7.6.0",[m
[32m+[m[32m        "convert-source-map": "^1.1.0",[m
[32m+[m[32m        "debug": "^4.1.0",[m
[32m+[m[32m        "json5": "^2.1.0",[m
[32m+[m[32m        "lodash": "^4.17.13",[m
[32m+[m[32m        "resolve": "^1.3.2",[m
[32m+[m[32m        "semver": "^5.4.1",[m
[32m+[m[32m        "source-map": "^0.5.0"[m
[32m+[m[32m      }[m
[32m+[m[32m    },[m
[32m+[m[32m    "@babel/generator": {[m
[32m+[m[32m      "version": "7.6.2",[m
[32m+[m[32m      "resolved": "https://registry.npm.taobao.org/@babel/generator/download/@babel/generator-7.6.2.tgz",[m
[32m+[m[32m      "integrity": "sha1-2sijwt8RgzTCop/zRG2hY2qPjAM=",[m
[32m+[m[32m      "dev": true,[m
[32m+[m[32m      "requires": {[m
[32m+[m[32m        "@babel/types": "^7.6.0",[m
[32m+[m[32m        "jsesc": "^2.5.1",[m
[32m+[m[32m        "lodash": "^4.17.13",[m
[32m+[m[32m        "source-map": "^0.5.0"[m
[32m+[m[32m      }[m
[32m+[m[32m    },[m
[32m+[m[32m    "@babel/helper-annotate-as-pure": {[m
[32m+[m[32m      "version": "7.0.0",[m
[32m+[m[32m      "resolved": "https://registry.npm.taobao.org/@babel/helper-annotate-as-pure/download/@babel/helper-annotate-as-pure-7.0.0.tgz",[m
[32m+[m[32m      "integrity": "sha1-Mj053QtQ4Qx8Bsp9djjmhk2MXDI=",[m
[32m+[m[32m      "dev": true,[m
[32m+[m[32m      "requires": {[m
[32m+[m[32m        "@babel/types": "^7.0.0"[m
[32m+[m[32m      }[m
[32m+[m[32m    },[m
[32m+[m[32m    "@babel/helper-builder-binary-assignment-operator-visitor": {[m
[32m+[m[32m      "version": "7.1.0",[m
[32m+[m[32m      "resolved": "https://registry.npm.taobao.org/@babel/helper-builder-binary-assignment-operator-visitor/download/@babel/helper-builder-binary-assignment-operator-visitor-7.1.0.tgz",[m
[32m+[m[32m      "integrity": "sha1-a2lijf5Ah3mODE7Zjj1Kay+9L18=",[m
[32m+[m[32m      "dev": true,[m
[32m+[m[32m      "requires": {[m
[32m+[m[32m        "@babel/helper-explode-assignable-expression": "^7.1.0",[m
[32m+[m[32m        "@babel/types": "^7.0.0"[m
[32m+[m[32m      }[m
[32m+[m[32m    },[m
[32m+[m[32m    "@babel/helper-call-delegate": {[m
[32m+[m[32m      "version": "7.4.4",[m
[32m+[m[32m      "resolved": "https://registry.npm.taobao.org/@babel/helper-call-delegate/download/@babel/helper-call-delegate-7.4.4.tgz",[m
[32m+[m[32m      "integrity": "sha1-h8H4yhmtVSpzanonscH8+LH/H0M=",[m
[32m+[m[32m      "dev": true,[m
[32m+[m[32m      "requires": {[m
[32m+[m[32m        "@babel/helper-hoist-variables": "^7.4.4",[m
[32m+[m[32m        "@babel/traverse": "^7.4.4",[m
[32m+[m[32m        "@babel/types": "^7.4.4"[m
[32m+[m[32m      }[m
[32m+[m[32m    },[m
[32m+[m[32m    "@babel/helper-create-class-features-plugin": {[m
[32m+[m[32m      "version": "7.6.0",[m
[32m+[m[32m      "resolved": "https://registry.npm.taobao.org/@babel/helper-create-class-features-plugin/download/@babel/helper-create-class-features-plugin-7.6.0.tgz?cache=0&other_urls=https%3A%2F%2Fregistry.npm.taobao.org%2F%40babel%2Fhelper-create-class-features-plugin%2Fdownload%2F%40babel%2Fhelper-create-class-features-plugin-7.6.0.tgz",[m
[32m+[m[32m      "integrity": "sha1-dpcRrMqIm+Nx6bwutoZB1VIYAh8=",[m
[32m+[m[32m      "dev": true,[m
[32m+[m[32m      "requires": {[m
[32m+[m[32m        "@babel/helper-function-name": "^7.1.0",[m
[32m+[m[32m        "@babel/helper-member-expression-to-functions": "^7.5.5",[m
[32m+[m[32m        "@babel/helper-optimise-call-expression": "^7.0.0",[m
[32m+[m[32m        "@babel/helper-plugin-utils": "^7.0.0",[m
[32m+[m[32m        "@babel/helper-replace-supers": "^7.5.5",[m
[32m+[m[32m        "@babel/helper-split-export-declaration": "^7.4.4"[m
[32m+[m[32m      }[m
[32m+[m[32m    },[m
[32m+[m[32m    "@babel/helper-define-map": {[m
[32m+[m[32m      "version": "7.5.5",[m
[32m+[m[32m      "resolved": "https://registry.npm.taobao.org/@babel/helper-define-map/download/@babel/helper-define-map-7.5.5.tgz",[m
[32m+[m[32m      "integrity": "sha1-PewywgRvN+CbKMk+sLED/Sol02k=",[m
[32m+[m[32m      "dev": true,[m
[32m+[m[32m      "requires": {[m
[32m+[m[32m        "@babel/helper-function-name": "^7.1.0",[m
[32m+[m[32m        "@babel/types": "^7.5.5",[m
[32m+[m[32m        "lodash": "^4.17.13"[m
[32m+[m[32m      }[m
[32m+[m[32m    },[m
[32m+[m[32m    "@babel/helper-explode-assignable-expression": {[m
[32m+[m[32m      "version": "7.1.0",[m
[32m+[m[32m      "resolved": "https://registry.npm.taobao.org/@babel/helper-explode-assignable-expression/download/@babel/helper-explode-assignable-expression-7.1.0.tgz",[m
[32m+[m[32m      "integrity": "sha1-U3+hP28WdN90WwwA7I/k6ZaByPY=",[m
[32m+[m[32m      "dev": true,[m
[32m+[m[32m      "requires": {[m
[32m+[m[32m        "@babel/traverse": "^7.1.0",[m
[32m+[m[32m        "@babel/types": "^7.0.0"[m
[32m+[m[32m      }[m
[32m+[m[32m    },[m
[32m+[m[32m    "@babel/helper-function-name": {[m
[32m+[m[32m      "version": "7.1.0",[m
[32m+[m[32m      "resolved": "https://registry.npm.taobao.org/@babel/helper-function-name/download/@babel/helper-function-name-7.1.0.tgz",[m
[32m+[m[32m      "integrity": "sha1-oM6wFoX3M1XUNgwSR/WCv6/I/1M=",[m
[32m+[m[32m      "dev": true,[m
[32m+[m[32m      "requires": {[m
[32m+[m[32m        "@babel/helper-get-function-arity": "^7.0.0",[m
[32m+[m[32m        "@babel/template": "^7.1.0",[m
[32m+[m[32m        "@babel/types": "^7.0.0"[m
[32m+[m[32m      }[m
[32m+[m[32m    },[m
[32m+[m[32m    "@babel/helper-get-function-arity": {[m
[32m+[m[32m      "version": "7.0.0",[m
[32m+[m[32m      "resolved": "https://registry.npm.taobao.org/@babel/helper-get-function-arity/download/@babel/helper-get-function-arity-7.0.0.tgz",[m
[32m+[m[32m      "integrity": "sha1-g1ctQyDipGVyY3NBE8QoaLZOScM=",[m
[32m+[m[32m      "dev": true,[m
[32m+[m[32m      "requires": {[m
[32m+[m[32m        "@babel/types": "^7.0.0"[m
[32m+[m[32m      }[m
[32m+[m[32m    },[m
[32m+[m[32m    "@babel/helper-hoist-variables": {[m
[32m+[m[32m      "version": "7.4.4",[m
[32m+[m[32m      "resolved": "https://registry.npm.taobao.org/@babel/helper-hoist-variables/download/@babel/helper-hoist-variables-7.4.4.tgz",[m
[32m+[m[32m      "integrity": "sha1-Api18lyMCcUxAtUqxKmPdz6yhQo=",[m
[32m+[m[32m      "dev": true,[m
[32m+[m[32m      "requires": {[m
[32m+[m[32m        "@babel/types": "^7.4.4"[m
[32m+[m[32m      }[m
[32m+[m[32m    },[m
[32m+[m[32m    "@babel/helper-member-expression-to-functions": {[m
[32m+[m[32m      "version": "7.5.5",[m
[32m+[m[32m      "resolved": "https://registry.npm.taobao.org/@babel/helper-member-expression-to-functions/download/@babel/helper-member-expression-to-functions-7.5.5.tgz?cache=0&other_urls=https%3A%2F%2Fregistry.npm.taobao.org%2F%40babel%2Fhelper-member-expression-to-functions%2Fdownload%2F%40babel%2Fhelper-member-expression-to-functions-7.5.5.tgz",[m
[32m+[m[32m      "integrity": "sha1-H7W47ERTqTxDnun+Ou6kqEt2tZA=",[m
[32m+[m[32m      "dev": true,[m
[32m+[m[32m      "requires": {[m
[32m+[m[32m        "@babel/types": "^7.5.5"[m
[32m+[m[32m      }[m
[32m+[m[32m    },[m
[32m+[m[32m    "@babel/helper-module-imports": {[m
[32m+[m[32m      "version": "7.0.0",[m
[32m+[m[32m      "resolved": "https://registry.npm.taobao.org/@babel/helper-module-imports/download/@babel/helper-module-imports-7.0.0.tgz",[m
[32m+[m[32m      "integrity": "sha1-lggbcRHkhtpNLNlxrRpP4hbMLj0=",[m
[32m+[m[32m      "dev": true,[m
[32m+[m[32m      "requires": {[m
[32m+[m[32m        "@babel/types": "^7.0.0"[m
[32m+[m[32m      }[m
[32m+[m[32m    },[m
[32m+[m[32m    "@babel/helper-module-transforms": {[m
[32m+[m[32m      "version": "7.5.5",[m
[32m+[m[32m      "resolved": "https://registry.npm.taobao.org/@babel/helper-module-transforms/download/@babel/helper-module-transforms-7.5.5.tgz?cache=0&other_urls=https%3A%2F%2Fregistry.npm.taobao.org%2F%40babel%2Fhelper-module-transforms%2Fdownload%2F%40babel%2Fhelper-module-transforms-7.5.5.tgz",[m
[32m+[m[32m      "integrity": "sha1-+E/4oJA43Lyh/UNVZhpQCTcWW0o=",[m
[32m+[m[32m      "dev": true,[m
[32m+[m[32m      "requires": {[m
[32m+[m[32m        "@babel/helper-module-imports": "^7.0.0",[m
[32m+[m[32m        "@babel/helper-simple-access": "^7.1.0",[m
[32m+[m[32m        "@babel/helper-split-export-declaration": "^7.4.4",[m
[32m+[m[32m        "@babel/template": "^7.4.4",[m
[32m+[m[32m        "@babel/types": "^7.5.5",[m
[32m+[m[32m        "lodash": "^4.17.13"[m
[32m+[m[32m      }[m
[32m+[m[32m    },[m
[32m+[m[32m    "@babel/helper-optimise-call-expression": {[m
[32m+[m[32m      "version": "7.0.0",[m
[32m+[m[32m      "resolved": "https://registry.npm.taobao.org/@babel/helper-optimise-call-expression/download/@babel/helper-optimise-call-expression-7.0.0.tgz",[m
[32m+[m[32m      "integrity": "sha1-opIMVwKwc8Fd5REGIAqoytIEl9U=",[m
[32m+[m[32m      "dev": true,[m
[32m+[m[32m      "requires": {[m
[32m+[m[32m        "@babel/types": "^7.0.0"[m
[32m+[m[32m      }[m
[32m+[m[32m    },[m
[32m+[m[32m    "@babel/helper-plugin-utils": {[m
[32m+[m[32m      "version": "7.0.0",[m
[32m+[m[32m      "resolved": "https://registry.npm.taobao.org/@babel/helper-plugin-utils/download/@babel/helper-plugin-utils-7.0.0.tgz",[m
[32m+[m[32m      "integrity": "sha1-u7P77phmHFaQNCN8wDlnupm08lA=",[m
[32m+[m[32m      "dev": true[m
[32m+[m[32m    },[m
[32m+[m[32m    "@babel/helper-regex": {[m
[32m+[m[32m      "version": "7.5.5",[m
[32m+[m[32m      "resolved": "https://registry.npm.taobao.org/@babel/helper-regex/download/@babel/helper-regex-7.5.5.tgz",[m
[32m+[m[32m      "integrity": "sha1-CqaCT3EAouDonBUnwjk2wVLKs1E=",[m
[32m+[m[32m      "dev": true,[m
[32m+[m[32m      "requires": {[m
[32m+[m[32m        "lodash": "^4.17.13"[m
[32m+[m[32m      }[m
[32m+[m[32m    },[m
[32m+[m[32m    "@babel/helper-remap-async-to-generator": {[m
[32m+[m[32m      "version": "7.1.0",[m
[32m+[m[32m      "resolved": "https://registry.npm.taobao.org/@babel/helper-remap-async-to-generator/download/@babel/helper-remap-async-to-generator-7.1.0.tgz",[m
[32m+[m[32m      "integrity": "sha1-Nh2AghtvONp1vT8HheziCojF/n8=",[m
[32m+[m[32m      "dev": true,[m
[32m+[m[32m      "requires": {[m
[32m+[m[32m        "@babel/helper-annotate-as-pure": "^7.0.0",[m
[32m+[m[32m        "@babel/helper-wrap-function": "^7.1.0",[m
[32m+[m[32m        "@babel/template": "^7.1.0",[m
[32m+[m[32m        "@babel/traverse": "^7.1.0",[m
[32m+[m[32m        "@babel/types": "^7.0.0"[m
[32m+[m[32m      }[m
[32m+[m[32m    },[m
[32m+[m[32m    "@babel/helper-replace-supers": {[m
[32m+[m[32m      "version": "7.5.5",[m
[32m+[m[32m      "resolved": "https://registry.npm.taobao.org/@babel/helper-replace-supers/download/@babel/helper-replace-supers-7.5.5.tgz?cache=0&other_urls=https%3A%2F%2Fregistry.npm.taobao.org%2F%40babel%2Fhelper-replace-supers%2Fdownload%2F%40babel%2Fhelper-replace-supers-7.5.5.tgz",[m
[32m+[m[32m      "integrity": "sha1-+EzkPfAxIi0rrQaNJibLV5nDS8I=",[m
[32m+[m[32m      "dev": true,[m
[32m+[m[32m      "requires": {[m
[32m+[m[32m        "@babel/helper-member-expression-to-functions": "^7.5.5",[m
[32m+[m[32m        "@babel/helper-optimise-call-expression": "^7.0.0",[m
[32m+[m[32m        "@babel/traverse": "^7.5.5",[m
[32m+[m[32m        "@babel/types": "^7.5.5"[m
[32m+[m[32m      }[m
[32m+[m[32m    },[m
[32m+[m[32m    "@babel/helper-simple-access": {[m
[32m+[m[32m      "version": "7.1.0",[m
[32m+[m[32m      "resolved": "https://registry.npm.taobao.org/@babel/helper-simple-access/download/@babel/helper-simple-access-7.1.0.tgz",[m
[32m+[m[32m      "integrity": "sha1-Ze65VMjCRb6qToWdphiPOdceWFw=",[m
[32m+[m[32m      "dev": true,[m
[32m+[m[32m      "requires": {[m
[32m+[m[32m        "@babel/template": "^7.1.0",[m
[32m+[m[32m        "@babel/types": "^7.0.0"[m
[32m+[m[32m      }[m
[32m+[m[32m    },[m
[32m+[m[32m    "@babel/helper-split-export-declaration": {[m
[32m+[m[32m      "version": "7.4.4",[m
[32m+[m[32m      "resolved": "https://registry.npm.taobao.org/@babel/helper-split-export-declaration/download/@babel/helper-split-export-declaration-7.4.4.tgz",[m
[32m+[m[32m      "integrity": "sha1-/5SJSjQL549T8GrwOLIFxJ2ZNnc=",[m
[32m+[m[32m      "dev": true,[m
[32m+[m[32m      "requires": {[m
[32m+[m[32m        "@babel/types": "^7.4.4"[m
[32m+[m[32m      }[m
[32m+[m[32m    },[m
[32m+[m[32m    "@babel/helper-wrap-function": {[m
[32m+[m[32m      "version": "7.2.0",[m
[32m+[m[32m      "resolved": "https://registry.npm.taobao.org/@babel/helper-wrap-function/download/@babel/helper-wrap-function-7.2.0.tgz",[m
[32m+[m[32m      "integrity": "sha1-xOABJEV2nigVtVKW6tQ6lYVJ9vo=",[m
[32m+[m[32m      "dev": true,[m
[32m+[m[32m      "requires": {[m
[32m+[m[32m        "@babel/helper-function-name": "^7.1.0",[m
[32m+[m[32m        "@babel/template": "^7.1.0",[m
[32m+[m[32m        "@babel/traverse": "^7.1.0",[m
[32m+[m[32m        "@babel/types": "^7.2.0"[m
[32m+[m[32m      }[m
[32m+[m[32m    },[m
[32m+[m[32m    "@babel/helpers": {[m
[32m+[m[32m      "version": "7.6.2",[m
[32m+[m[32m      "resolved": "https://registry.npm.taobao.org/@babel/helpers/download/@babel/helpers-7.6.2.tgz",[m
[32m+[m[32m      "integrity": "sha1-aB/+SJ6k3MVfI85GnljlnBwEUVM=",[m
[32m+[m[32m      "dev": true,[m
[32m+[m[32m      "requires": {[m
[32m+[m[32m        "@babel/template": "^7.6.0",[m
[32m+[m[32m        "@babel/traverse": "^7.6.2",[m
[32m+[m[32m        "@babel/types": "^7.6.0"[m
[32m+[m[32m      }[m
[32m+[m[32m    },[m
[32m+[m[32m    "@babel/highlight": {[m
[32m+[m[32m      "version": "7.5.0",[m
[32m+[m[32m      "resolved": "https://registry.npm.taobao.org/@babel/highlight/download/@babel/highlight-7.5.0.tgz",[m
[32m+[m[32m      "integrity": "sha1-VtETEr2SSPphlZHQJHK+boyzJUA=",[m
[32m+[m[32m      "dev": true,[m
[32m+[m[32m      "requires": {[m
[32m+[m[32m        "chalk": "^2.0.0",[m
[32m+[m[32m        "esutils": "^2.0.2",[m
[32m+[m[32m        "js-tokens": "^4.0.0"[m
[32m+[m[32m      }[m
[32m+[m[32m    },[m
[32m+[m[32m    "@babel/parser": {[m
[32m+[m[32m      "version": "7.6.2",[m
[32m+[m[32m      "resolved": "https://registry.npm.taobao.org/@babel/parser/download/@babel/parser-7.6.2.tgz?cache=0&sync_timestamp=1569286295205&other_urls=https%3A%2F%2Fregistry.npm.taobao.org%2F%40babel%2Fparser%2Fdownload%2F%40babel%2Fparser-7.6.2.tgz",[m
[32m+[m[32m      "integrity": "sha1-IF6cleFro7i5YJBnemfJ1gdbcKE=",[m
[32m+[m[32m      "dev": true[m
[32m+[m[32m    },[m
[32m+[m[32m    "@babel/plugin-proposal-async-generator-functions": {[m
[32m+[m[32m      "version": "7.2.0",[m
[32m+[m[32m      "resolved": "https://registry.npm.taobao.org/@babel/plugin-proposal-async-generator-functions/download/@babel/plugin-proposal-async-generator-functions-7.2.0.tgz",[m
[32m+[m[32m      "integrity": "sha1-somzBmadzkrSCwJSiJoVdoydQX4=",[m
[32m+[m[32m      "dev": true,[m
[32m+[m[32m      "requires": {[m
[32m+[m[32m        "@babel/helper-plugin-utils": "^7.0.0",[m
[32m+[m[32m        "@babel/helper-remap-async-to-generator": "^7.1.0",[m
[32m+[m[32m        "@babel/plugin-syntax-async-generators": "^7.2.0"[m
[32m+[m[32m      }[m
[32m+[m[32m    },[m
[32m+[m[32m    "@babel/plugin-proposal-class-properties": {[m
[32m+[m[32m      "version": "7.5.5",[m
[32m+[m[32m      "resolved": "https://registry.npm.taobao.org/@babel/plugin-proposal-class-properties/download/@babel/plugin-proposal-class-properties-7.5.5.tgz",[m
[32m+[m[32m      "integrity": "sha1-qXTPrh43wxEOcfPGouSLjnGVjNQ=",[m
[32m+[m[32m      "dev": true,[m
[32m+[m[32m      "requires": {[m
[32m+[m[32m        "@babel/helper-create-class-features-plugin": "^7.5.5",[m
[32m+[m[32m        "@babel/helper-plugin-utils": "^7.0.0"[m
[32m+[m[32m      }[m
[32m+[m[32m    },[m
[32m+[m[32m    "@babel/plugin-proposal-decorators": {[m
[32m+[m[32m      "version": "7.6.0",[m
[32m+[m[32m      "resolved": "https:/