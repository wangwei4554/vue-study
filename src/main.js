import Vue from 'vue'        //导入的是vue的核心
import App from './App.vue'  //前面有./的是项目本身拥有的.vue组件
import './plugins/element.js'
import router from './router'
import store from './store'

Vue.config.productionTip = false
//修改Vue的原型
Vue.prototype.$bus = new Vue();//这个的意思是，在原型中加了一个属性叫做$bus,这个$bus也是一个vue对象

new Vue({
    router,
    store,

    //渲染的是App这个组件页面
    render: h => h(App)
}).$mount('#app')        //挂载到index中id=app的宿主上
