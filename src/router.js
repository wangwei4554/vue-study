import Vue from 'vue'
import Router from 'vue-router'
import Page1 from './views/Page1.vue';
import Page2 from './views/Page2.vue';
//插件挂载
Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
      {path:"/page1",component:Page1},
      {path:'/page2/:id/:msg', name:"page2" ,component:Page2}
  ]
})
